package com.example.mohan.loginact;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class Adminoruser extends AppCompatActivity {

    Button member,leader;


    ProgressBar loading;
    FirebaseAuth mAuth;
    DatabaseReference databaseReference;

    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference myRef;
    private  String userID;

    //Shared Preferences
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adminoruser);
        member = (Button)findViewById(R.id.TeamMember);
        leader = (Button)findViewById(R.id.TeamLeader);

        progress = new ProgressDialog(this);

        databaseReference = FirebaseDatabase.getInstance().getReference("Adminoruser");
        mAuth = FirebaseAuth.getInstance();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();


        member.setVisibility(View.GONE);
        leader.setVisibility(View.GONE);

        start();


        member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                member();
            }
        });

        leader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                leader();
            }
        });


    }

    protected void start() {


        mAuth = FirebaseAuth.getInstance();
        myRef = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = mAuth.getCurrentUser();
        userID = user.getUid();
        progress = ProgressDialog.show(this, "Please wait...", "Loading...", true);

       myRef.child("Adminoruser").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                FirebaseUser user = mAuth.getCurrentUser();
                userID = user.getUid();


                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    if (ds.getKey().equals(userID)) {

                        if(ds.getValue().equals("TeamMember")) {

                            finish();
                            startActivity(new Intent(getApplicationContext(), MemberSignup.class));

                        }
                        else {
                            finish();
                            startActivity(new Intent(getApplicationContext(), Leadersignup.class));
                        }


                    }
                }
                progress.dismiss();
                member.setVisibility(View.VISIBLE);
                leader.setVisibility(View.VISIBLE);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        String tl = sharedPreferences.getString(getString(R.string.TeamLeader),"False");
        String tn = sharedPreferences.getString(getString(R.string.TeamMember),"False");

        if(tl.equals("True")) {
            finish();
            startActivity(new Intent(getApplicationContext(), Leadersignup.class));
        }
        else
        if (tn.equals("True")) {
            finish();
            startActivity(new Intent(getApplicationContext(), MemberSignup.class));
        }

       // progress.dismiss();

    }

    private void leader() {

        FirebaseUser user = mAuth.getCurrentUser();
        String id = user.getUid();
        databaseReference.child(id).setValue("TeamLeader");

        finish();
        startActivity(new Intent(this,Leadersignup.class));

    }

    private void member() {

        final FirebaseUser user1 = mAuth.getCurrentUser();

        FirebaseUser user = mAuth.getCurrentUser();
        String id = user.getUid();
        databaseReference.child(id).setValue("TeamMember");

        finish();
        startActivity(new Intent(this,MemberSignup.class));

    }
}
