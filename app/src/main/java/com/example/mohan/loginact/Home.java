package com.example.mohan.loginact;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Home extends AppCompatActivity {

    ImageView fixapp;
    TextView result;
    GPSTracker gps;
    double longitude,latitude = 0.0;
    TextView distanceandtime,appointmentdetails;
    DatabaseReference myRef;
    FirebaseAuth mAuth;
    String pid;
    String formattedDate;
    String did,time,tokeno,date;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.menuLogout:
                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(new Intent(this,MainActivity.class));
                break;
        }
        return true;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        final MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logout,menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home2);
        fixapp = (ImageView) findViewById(R.id.fixappointment);
        result = (TextView) findViewById(R.id.result);
      //  distanceandtime = (TextView) findViewById(R.id.distanceandtime);
        appointmentdetails = (TextView) findViewById(R.id.Appoinment_details);
        myRef = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        pid = user.getUid();


        startService(new Intent(this,sendgpsservice.class));
        Date c = Calendar.getInstance().getTime();

        SimpleDateFormat df = new SimpleDateFormat("MM-dd-yy");
        formattedDate = df.format(c);

        myRef.child("PatientAppointment").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                show_appoinment(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        appointmentdetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), View_appoinment.class);

                intent.putExtra("date",date);
                intent.putExtra("time",time);
                intent.putExtra("tokeno",tokeno);
                intent.putExtra("did",did);

                startActivity(intent);


            }
        });

        fixapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),fixappointment.class));
            }
        });



    }

    private void show_appoinment(DataSnapshot dataSnapshot) {

        for(DataSnapshot ds : dataSnapshot.getChildren()) {


            if(ds.getKey().equals(pid))
            {
                for(DataSnapshot temp1 : ds.getChildren())
                {
                    appointmentdetails.setText(temp1.getKey());

                    if(temp1.getKey().equals(formattedDate))
                    {
                        appointmentdetails.setVisibility(View.VISIBLE);
                        patientappoinmentdetials detials = new patientappoinmentdetials();
                        detials.setDate(temp1.getValue(patientappoinmentdetials.class).getDate());
                        detials.setTime(temp1.getValue(patientappoinmentdetials.class).getTime());
                        detials.setTokeno(temp1.getValue(patientappoinmentdetials.class).getTokeno());
                        detials.setDid(temp1.getValue(patientappoinmentdetials.class).getDid());

                        date = detials.getDate();
                        time = detials.getTime();
                        tokeno = detials.getTokeno();
                        did = detials.getDid();

                        appointmentdetails.setVisibility(View.VISIBLE);
                        appointmentdetails.setText("Date : "+detials.getDate()+"\nTime : "+detials.getTime()+"\nTokenno : "+detials.getTokeno());
                        return;
                    }

                }

            }


        }

    }


}
