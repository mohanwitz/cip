package com.example.mohan.loginact;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LeaderUpdateProfile extends AppCompatActivity {

    EditText name,Teamsize,Location,Phoneno,Teamemail;
    TextView TeamName;
    Button update;

    FirebaseAuth mAuth;
    DatabaseReference databaseReference;

    private FirebaseDatabase mFirebaseDatabase;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference myRef;
    private  String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leaderupdate);

        update  = (Button)findViewById(R.id.update);
        name  = (EditText)findViewById(R.id.name);
        TeamName  = (TextView) findViewById(R.id.Teamname);
        Teamsize  = (EditText)findViewById(R.id.Teamsize);
        Location  = (EditText)findViewById(R.id.Location);
        Phoneno  = (EditText)findViewById(R.id.Phoneno);
        Teamemail  = (EditText)findViewById(R.id.Teamemail);


        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference();
        FirebaseUser user = mAuth.getCurrentUser();
        userID = user.getUid();

        mAuth = FirebaseAuth.getInstance();

        update();

    }

    private void update() {

        myRef.child("TeamLeaders").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                showData(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void showData(DataSnapshot dataSnapshot) {

        FirebaseUser user = mAuth.getCurrentUser();

        String id = user.getUid();
        for(DataSnapshot ds : dataSnapshot.getChildren()){

            if(ds.getValue(UserInformation.class).userid.equals(id)) {
                UserInformation uInfo = new UserInformation();
                uInfo.setDispname(ds.getValue(UserInformation.class).getDispname()); //set the name
                uInfo.setLoc(ds.getValue(UserInformation.class).getLoc()); //set the email
                uInfo.setPno(ds.getValue(UserInformation.class).getPno()); //set the phone_num
                uInfo.setTm(ds.getValue(UserInformation.class).getTm());
                uInfo.setTn(ds.getValue(UserInformation.class).getTn());
                uInfo.setTs(ds.getValue(UserInformation.class).getTs());


                name.setText(uInfo.getDispname());
                Location.setText(uInfo.getLoc());
                Phoneno.setText(uInfo.getPno());
                TeamName.setText(uInfo.getTn());
                Teamemail.setText(uInfo.getTm());
                Teamsize.setText(uInfo.getTs());
            }
            update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    updatepof();
                }
            });

        }

    }

    private void updatepof() {

        String aname = name.getText().toString();
        String teamname = TeamName.getText().toString();
        String teamsize = Teamsize.getText().toString();
        String location = Location.getText().toString();
        String phoneno = Phoneno.getText().toString();
        String teamemail = Teamemail.getText().toString();


        if(aname.isEmpty())
        {
            name.setError("Please enter the name");
            name.requestFocus();
            return;
        }

        if(teamname.isEmpty())
        {
            TeamName.setError("Please enter the team name");
            TeamName.requestFocus();
            return;
        }

        if(teamsize.isEmpty())
        {
            Teamsize.setError("Please enter the team Size");
            Teamsize.requestFocus();
            return;
        }

        if(location.isEmpty())
        {
            Location.setError("Please enter your location");
            Location.requestFocus();
            return;
        }

        if(phoneno.isEmpty())
        {
            Phoneno.setError("Please enter your Phone number");
            Phoneno.requestFocus();
            return;
        }

        if(teamemail.isEmpty())
        {
            Teamemail.setError("Please enter your Team Email");
            Teamemail.requestFocus();
            return;
        }

        FirebaseUser user = mAuth.getCurrentUser();

        String id = user.getUid();

        databaseReference = FirebaseDatabase.getInstance().getReference("Admins").child(id);

        Profile prof = new Profile(aname,teamname,teamsize,location,teamemail,phoneno,id);

        databaseReference.setValue(prof).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(getApplicationContext(),"Profile updated Successfully",Toast.LENGTH_SHORT).show();
            }
        });


    }

}
