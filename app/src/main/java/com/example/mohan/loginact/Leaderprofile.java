package com.example.mohan.loginact;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Leaderprofile extends AppCompatActivity {

    ImageView image;
    TextView verify;
    FirebaseAuth mAuth;
    Button updatebut;

    private static final String TAG = "ViewDatabase";

    //add Firebase Database stuff

    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference myRef;
    private  String userID;

    private ListView mListView;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        startActivity(new Intent(getApplicationContext(), Home.class));

    }

    @Override
    protected void onStart() {

        super.onStart();

        mAuth.addAuthStateListener(mAuthListener);

        final FirebaseUser user = mAuth.getCurrentUser();

        verify.setVisibility(View.VISIBLE);

        if(!user.isEmailVerified()) {

            verify.setText("Email is not verified(Click here to verify)");
        }

        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful())
                        {
                            Toast.makeText(getApplicationContext(),"Email verification is sent",Toast.LENGTH_SHORT).show();
                            finish();
                            startActivity(getIntent());
                        }
                    }
                });
            }
        });


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mAuth = FirebaseAuth.getInstance();

        image = (ImageView)findViewById(R.id.image);
        verify = (TextView)findViewById(R.id.verifyemail);
        updatebut = (Button)findViewById(R.id.Update);

        mListView = (ListView) findViewById(R.id.listview);

        //declare the database reference object. This is what we use to access the database.
        //NOTE: Unless you are signed in, this will not be useable.
        mAuth = FirebaseAuth.getInstance();
        myRef = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = mAuth.getCurrentUser();
        userID = user.getUid();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());

                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };

        loadinformation();

        myRef.child("TeamLeaders").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                showData(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void showData(DataSnapshot dataSnapshot) {
        for(DataSnapshot ds : dataSnapshot.getChildren()) {

            FirebaseUser user = mAuth.getCurrentUser();
            userID = user.getUid();

            if (ds.getValue(UserInformation.class).userid.equals(userID)) {

                UserInformation uInfo = new UserInformation();
                uInfo.setDispname(ds.getValue(UserInformation.class).getDispname()); //set the name
                uInfo.setLoc(ds.getValue(UserInformation.class).getLoc()); //set the email
                uInfo.setPno(ds.getValue(UserInformation.class).getPno()); //set the phone_num
                uInfo.setTm(ds.getValue(UserInformation.class).getTm());
                uInfo.setTn(ds.getValue(UserInformation.class).getTn());
                uInfo.setTs(ds.getValue(UserInformation.class).getTs());

                ArrayList<String> array = new ArrayList<>();
                array.add(uInfo.getDispname());
                array.add(uInfo.getLoc());
                array.add(uInfo.getPno());
                array.add(uInfo.getTm());
                array.add(uInfo.getTn());
                array.add(uInfo.getTs());
                ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, array);
                mListView.setAdapter(adapter);
            }
        }
        updatebut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateprofile();
            }
        });
    }

    private void updateprofile() {

        startActivity(new Intent(this,LeaderUpdateProfile.class));
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {

        final MenuInflater inflater = getMenuInflater();
        mAuth = FirebaseAuth.getInstance();
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = mAuth.getCurrentUser();
        final String[] userID = {user.getUid()};

        myRef.child("Adminoruser").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                FirebaseUser user = mAuth.getCurrentUser();
                userID[0] = user.getUid();


                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    if (ds.getKey().equals(userID[0])) {

                        if(ds.getValue().equals("TeamMember")) {

                            inflater.inflate(R.menu.menu,menu);

                        }
                        else {
                            inflater.inflate(R.menu.leadermenu,menu);
                        }


                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case R.id.menuLogout:
                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(new Intent(this,MainActivity.class));
                break;

            case R.id.Profile:

                startActivity(new Intent(this,helperprofile.class));
                break;
            case R.id.Mythreads:

                startActivity(new Intent(getApplicationContext(),MyThread.class));
                break;

            case R.id.scheduled:
                startActivity(new Intent(getApplicationContext(),Scheduled_Threads.class));
                break;
            case R.id.Help:
                startActivity(new Intent(getApplicationContext(),Help.class));
                break;

        }
        return true;
    }




    private void loadinformation() {

        final FirebaseUser user = mAuth.getCurrentUser();
        if(!user.isEmailVerified())
            return;

        image.setVisibility(View.VISIBLE);
        verify.setVisibility(View.VISIBLE);


        if(user != null) {

            if(user.getPhotoUrl() != null) {
                Glide.with(this)
                        .load(user.getPhotoUrl().toString())
                        .into(image);
            }
            else
                return;


            verify.setVisibility(View.VISIBLE);

            if(user.isEmailVerified()) {

                verify.setText("Email is verified");
            }

        }
    }
}
