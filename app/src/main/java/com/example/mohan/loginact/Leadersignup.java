package com.example.mohan.loginact;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Leadersignup extends AppCompatActivity {

    private static final int CHOOSE_IMAGE = 102 ;
    ImageView image;
    EditText name,TeamName,Teamsize,Location,Phoneno,Teamemail;
    TextView verify;
    Button save;
    ProgressBar pb;
    Uri profileimage;
    String imagedownloadurl;
    TextView verifiedname;
    FirebaseAuth mAuth;
    ProgressDialog progress;

    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference myRef;
    private  String userID;


    DatabaseReference databaseReference;
    LinearLayout parent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leadersignup);

        parent = (LinearLayout)findViewById(R.id.parent);
        progress = new ProgressDialog(this);

        databaseReference = FirebaseDatabase.getInstance().getReference("TeamLeaders");
        image = (ImageView)findViewById(R.id.imageView);
        save  = (Button)findViewById(R.id.update);
        pb    = (ProgressBar)findViewById(R.id.imagepb);
        name  = (EditText)findViewById(R.id.name);
        TeamName  = (EditText)findViewById(R.id.Teamname);
        Teamsize  = (EditText)findViewById(R.id.Teamsize);
        Location  = (EditText)findViewById(R.id.Location);
        Phoneno  = (EditText)findViewById(R.id.Phoneno);
        Teamemail  = (EditText)findViewById(R.id.Teamemail);


        verify = (TextView)findViewById(R.id.verify);
        verifiedname = (TextView)findViewById(R.id.verifiedname);

        mAuth = FirebaseAuth.getInstance();

        parent.setVisibility(View.GONE);
        Start();

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imagechooser();

            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    addprofile();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    protected void Start() {

        /*image.setVisibility(View.GONE);
        save.setVisibility(View.GONE);
        pb.setVisibility(View.GONE);
        name.setVisibility(View.GONE);
        TeamName.setVisibility(View.GONE);
        Teamsize.setVisibility(View.GONE);
        Location.setVisibility(View.GONE);
        Phoneno.setVisibility(View.GONE);
        Teamemail.setVisibility(View.GONE);
        verify.setVisibility(View.GONE);
        verifiedname.setVisibility(View.GONE);*/

        if(mAuth.getCurrentUser() == null)
        {
            finish();
            startActivity(new Intent(this,MainActivity.class));
        }

        mAuth = FirebaseAuth.getInstance();
        myRef = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = mAuth.getCurrentUser();
        userID = user.getUid();

        progress = ProgressDialog.show(this, "Please wait...", "Loading...", true);
        myRef.child("TeamLeaders").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                FirebaseUser user = mAuth.getCurrentUser();
                userID = user.getUid();

                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    if (ds.getValue(UserInformation.class).userid.equals(userID)) {


                        finish();
                        startActivity(new Intent(getApplicationContext(),Home.class));

                    }
                }
                progress.dismiss();
                parent.setVisibility(View.VISIBLE);
                /*image.setVisibility(View.VISIBLE);
                save.setVisibility(View.VISIBLE);
                pb.setVisibility(View.VISIBLE);
                name.setVisibility(View.VISIBLE);
                TeamName.setVisibility(View.VISIBLE);
                Teamsize.setVisibility(View.VISIBLE);
                Location.setVisibility(View.VISIBLE);
                Phoneno.setVisibility(View.VISIBLE);
                Teamemail.setVisibility(View.VISIBLE);
                verify.setVisibility(View.VISIBLE);
                verifiedname.setVisibility(View.VISIBLE);*/

            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void addprofile() throws InterruptedException {


        String aname = name.getText().toString();
        String teamname = TeamName.getText().toString();
        String teamsize = Teamsize.getText().toString();
        String location = Location.getText().toString();
        String phoneno = Phoneno.getText().toString();
        String teamemail = Teamemail.getText().toString();


        if(aname.isEmpty())
        {
            name.setError("Please enter the name");
            name.requestFocus();
            return;
        }

        if(teamname.isEmpty())
        {
            TeamName.setError("Please enter the team name");
            TeamName.requestFocus();
            return;
        }

        if(teamsize.isEmpty())
        {
            Teamsize.setError("Please enter the team Size");
            Teamsize.requestFocus();
            return;
        }

        if(location.isEmpty())
        {
            Location.setError("Please enter your location");
            Location.requestFocus();
            return;
        }

        if(phoneno.isEmpty())
        {
            Phoneno.setError("Please enter your Phone number");
            Phoneno.requestFocus();
            return;
        }

        if(teamemail.isEmpty())
        {
            Teamemail.setError("Please enter your Team Email");
            Teamemail.requestFocus();
            return;
        }


        FirebaseUser user = mAuth.getCurrentUser();

        if(user != null && imagedownloadurl != null)
        {
            UserProfileChangeRequest profile = new UserProfileChangeRequest.Builder()
                    .setDisplayName(aname)
                    .setPhotoUri(Uri.parse(imagedownloadurl))
                    .build();
            user.updateProfile(profile).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful())
                    {
                        Toast.makeText(getApplicationContext(),"Profile updated",Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }


        String id = user.getUid();


        Profile prof = new Profile(aname,teamname,teamsize,location,teamemail,phoneno,id);

        databaseReference.child(id).setValue(prof);

        databaseReference = FirebaseDatabase.getInstance().getReference("Teamnames");

        databaseReference.child(id).setValue(teamname);

        TimeUnit.SECONDS.sleep(1);
        finish();
        startActivity(new Intent(this,Home.class));

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CHOOSE_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            profileimage = data.getData();

            try {

                Bitmap bp = MediaStore.Images.Media.getBitmap(getContentResolver(), profileimage);
                image.setImageBitmap(bp);

                uploadimagetofb();

            } catch (IOException e) {

                e.printStackTrace();
            }
        }

    }

    private void uploadimagetofb() {

        StorageReference imagereference =
                FirebaseStorage.getInstance().getReference("profilepics/"+System.currentTimeMillis()+".jpeg");



        if(profileimage != null)
        {
            pb.setVisibility(View.VISIBLE);
            imagereference.putFile(profileimage).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    pb.setVisibility(View.GONE);
                    imagedownloadurl = taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                    pb.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();
                }
            });
        }
    }

   @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;
    }

   @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case R.id.menuLogout:
                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(new Intent(this,MainActivity.class));
                break;
        }
        return true;
    }

    private void imagechooser()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent ,"Select a Photo"),CHOOSE_IMAGE);
    }
}
