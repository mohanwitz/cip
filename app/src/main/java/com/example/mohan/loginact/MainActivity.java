package com.example.mohan.loginact;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    EditText username, password;
    private FirebaseAuth mAuth;
    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username = (EditText)findViewById(R.id.editTextEmail);
        password = (EditText)findViewById(R.id.editTextPassword);
        pb = (ProgressBar)findViewById(R.id.Loginpb);

        findViewById(R.id.buttonLogin).setOnClickListener(this);
        mAuth = FirebaseAuth.getInstance();
        findViewById(R.id.textViewSignup).setOnClickListener(this);

    }


    private void userlogin()
    {
        String email = username.getText().toString().trim();
        String pass  = password.getText().toString().trim();

        //To check for Email erros
        if(email.isEmpty())
        {
            username.setError("Email is required");
            username.requestFocus();
            return;
        }

        //To check for the correct email
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            username.setError("Please enter a valid email");
            username.requestFocus();
            return;
        }

        //To check for the Password errors
        if(pass.isEmpty())
        {
            password.setError("Password is required");
            password.requestFocus();
            return;
        }

        //Pass should be a min of 6 char

        if(pass.length()<6)
        {
            password.setError("Minimum length of password should be atleast 6");
            password.requestFocus();
            return;
        }

        //Creation of User

        pb.setVisibility(View.VISIBLE);

        mAuth.signInWithEmailAndPassword(email,pass).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                pb.setVisibility(View.GONE);

                if(task.isSuccessful())
                {
                    finish();
                    Intent intent = new Intent(MainActivity.this,MemberSignup.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);


                }
                else
                {
                    Toast.makeText(getApplicationContext(),task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(mAuth.getCurrentUser() != null)
        {
            finish();
            startActivity(new Intent(this,MemberSignup.class));

        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.textViewSignup:

                finish();
                startActivity(new Intent(this, Signupactivity.class));
                break;

            case R.id.buttonLogin:

                userlogin();
                break;

        }

    }
}
