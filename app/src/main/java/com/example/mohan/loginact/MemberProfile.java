package com.example.mohan.loginact;

public class MemberProfile {

    String fname;
    String lname;
    String age;
    String gender;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public MemberProfile(String fname, String lname, String age, String gender, String location, String phoneno, String userid) {
        this.fname = fname;
        this.lname = lname;
        this.age = age;
        this.gender = gender;
        this.location = location;
        this.phoneno = phoneno;
        this.userid = userid;
    }

    String location;
    String phoneno;
    String userid;

    public MemberProfile(String fname, String lname, String age, String location, String phoneno, String userid) {
        this.fname = fname;
        this.lname = lname;
        this.age = age;
        this.location = location;
        this.phoneno = phoneno;
        this.userid = userid;
    }

    public MemberProfile(){

    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public String getAge() {
        return age;
    }

    public String getLocation() {
        return location;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public String getUserid() {
        return userid;
    }
}
