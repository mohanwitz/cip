package com.example.mohan.loginact;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class MemberSignup extends AppCompatActivity {


    private static final int CHOOSE_IMAGE = 102 ;
    ImageView image;
    EditText fname,lname,age,Location,Phoneno;
    EditText editText;
    Button save;
    ProgressBar pb;
    Uri profileimage;
    String imagedownloadurl;
    FirebaseAuth mAuth;
    ProgressDialog progress;
    LinearLayout parent;

    FirebaseAuth.AuthStateListener mAuthListener;
    DatabaseReference databaseReference,myRef;
    String userID;
    final String[] tempteamname = new String[1];

    final Calendar myCalendar = Calendar.getInstance();
    private EditText edittext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_signup);

        mAuth = FirebaseAuth.getInstance();

        //mAuth = FirebaseAuth.getInstance();
        myRef = FirebaseDatabase.getInstance().getReference();

        FirebaseUser user = mAuth.getCurrentUser();
        userID = user.getUid();

        parent = (LinearLayout)findViewById(R.id.Teamdropdown);
        image = (ImageView)findViewById(R.id.imageView);
        save  = (Button)findViewById(R.id.update);
        pb    = (ProgressBar)findViewById(R.id.imagepb);
        fname  = (EditText)findViewById(R.id.fname);
        lname = (EditText)findViewById(R.id.lname);

        Location  = (EditText)findViewById(R.id.Location);
        Phoneno  = (EditText)findViewById(R.id.Phoneno);

        edittext = (EditText) findViewById(R.id.age);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addprofile();
            }
        });

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        parent.setVisibility(View.GONE);

        Start();


        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imagechooser();

            }
        });

        edittext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(MemberSignup.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });







    }

    private void updateLabel() {
        String myFormat = "MM-dd-yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        edittext.setText(sdf.format(myCalendar.getTime()));
    }



    protected void Start() {

        if(mAuth.getCurrentUser() == null)
        {
            finish();
            startActivity(new Intent(this,MainActivity.class));
        }

        progress = ProgressDialog.show(this, "Please wait...", "Loading...", true);
        myRef.child("Clients").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                FirebaseUser user = mAuth.getCurrentUser();
                userID = user.getUid();

                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    if (ds.getValue(MemberProfile.class).userid.equals(userID)) {


                        finish();
                        startActivity(new Intent(getApplicationContext(),Home.class));

                    }
                }
                progress.dismiss();
                parent.setVisibility(View.VISIBLE);
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void addprofile() {


        String fnam = fname.getText().toString();
        String lnam = lname.getText().toString();
        String a = edittext.getText().toString();
        String location = Location.getText().toString();
        String phoneno = Phoneno.getText().toString();


        String tn = tempteamname[0];

        if(fnam.isEmpty())
        {
            fname.setError("Please enter the name");
            fname.requestFocus();
            return;
        }

        if(lnam.isEmpty())
        {
            lname.setError("Please enter the name");
            lname.requestFocus();
            return;
        }

        if(a.isEmpty())
        {
            edittext.setError("Please enter the name");
            edittext.requestFocus();
            return;
        }


        if(location.isEmpty())
        {
            Location.setError("Please enter your location");
            Location.requestFocus();
            return;
        }

        if(phoneno.isEmpty())
        {
            Phoneno.setError("Please enter your Phone number");
            Phoneno.requestFocus();
            return;
        }


        FirebaseUser user = mAuth.getCurrentUser();

        if(user != null && imagedownloadurl != null)
        {
            UserProfileChangeRequest profile = new UserProfileChangeRequest.Builder()
                    .setDisplayName(fnam)
                    .setPhotoUri(Uri.parse(imagedownloadurl))
                    .build();
            user.updateProfile(profile).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful())
                    {
                        Toast.makeText(getApplicationContext(),"Profile updated",Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }


        String id = user.getUid();

        MemberProfile prof = new MemberProfile(fnam,lnam,a,location,phoneno,id);

        databaseReference = FirebaseDatabase.getInstance().getReference("Clients");

        databaseReference.child(id).setValue(prof);

        Toast.makeText(getApplicationContext(),"Inserted into the database",Toast.LENGTH_LONG).show();

        finish();
        startActivity(new Intent(this,Home.class));

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CHOOSE_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            profileimage = data.getData();

            try {

                Bitmap bp = MediaStore.Images.Media.getBitmap(getContentResolver(), profileimage);
                image.setImageBitmap(bp);

                uploadimagetofb();

            } catch (IOException e) {

                e.printStackTrace();
            }
        }

    }

    private void uploadimagetofb() {

        StorageReference imagereference =
                FirebaseStorage.getInstance().getReference("profilepics/"+System.currentTimeMillis()+".jpeg");



        if(profileimage != null)
        {
            pb.setVisibility(View.VISIBLE);
            imagereference.putFile(profileimage).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    pb.setVisibility(View.GONE);
                    imagedownloadurl = taskSnapshot.getMetadata().getReference().getDownloadUrl().toString();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                    pb.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case R.id.menuLogout:
                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(new Intent(this,MainActivity.class));
                break;
        }
        return true;
    }

    private void imagechooser()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent ,"Select a Photo"),CHOOSE_IMAGE);
    }



}