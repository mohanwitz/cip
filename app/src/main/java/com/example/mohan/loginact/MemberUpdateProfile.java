package com.example.mohan.loginact;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MemberUpdateProfile extends AppCompatActivity {

    TextView TeamName;
    EditText name,Location,Phoneno;
    Button update;

    FirebaseAuth mAuth;
    DatabaseReference databaseReference;

    private FirebaseDatabase mFirebaseDatabase;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference myRef;
    private  String userID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_update_profile);

        update  = (Button)findViewById(R.id.update);
        name  = (EditText)findViewById(R.id.name);
        TeamName  = (TextView) findViewById(R.id.teamname);
        Location  = (EditText)findViewById(R.id.Location);
        Phoneno  = (EditText)findViewById(R.id.Phoneno);


        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference();
        FirebaseUser user = mAuth.getCurrentUser();
        userID = user.getUid();

        mAuth = FirebaseAuth.getInstance();

        update();

    }

    private void update() {

        myRef.child("TeamMembers").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                showData(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void showData(DataSnapshot dataSnapshot) {

        FirebaseUser user = mAuth.getCurrentUser();

        String id = user.getUid();
        for(DataSnapshot ds : dataSnapshot.getChildren()){

            if(ds.getValue(MemberInformation.class).userid.equals(id)) {

                MemberInformation uInfo = new MemberInformation();
                uInfo.setDispname(ds.getValue(MemberInformation.class).getDispname()); //set the name
                uInfo.setLoc(ds.getValue(MemberInformation.class).getLoc()); //set the email
                uInfo.setPno(ds.getValue(MemberInformation.class).getPno()); //set the phone_num
                uInfo.setTn(ds.getValue(MemberInformation.class).getTn());


                name.setText(uInfo.getDispname());
                Location.setText(uInfo.getLoc());
                Phoneno.setText(uInfo.getPno());
                TeamName.setText(uInfo.getTn());

            }
            update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    updatepof();
                }
            });

        }

    }

    private void updatepof() {

        String aname = name.getText().toString();
        String teamname = TeamName.getText().toString();
        String location = Location.getText().toString();
        String phoneno = Phoneno.getText().toString();


        if(aname.isEmpty())
        {
            name.setError("Please enter the name");
            name.requestFocus();
            return;
        }

        if(teamname.isEmpty())
        {
            TeamName.setError("Please enter the team name");
            TeamName.requestFocus();
            return;
        }


        if(location.isEmpty())
        {
            Location.setError("Please enter your location");
            Location.requestFocus();
            return;
        }

        if(phoneno.isEmpty())
        {
            Phoneno.setError("Please enter your Phone number");
            Phoneno.requestFocus();
            return;
        }

        FirebaseUser user = mAuth.getCurrentUser();

        String id = user.getUid();

        databaseReference = FirebaseDatabase.getInstance().getReference("TeamMembers").child(id);

     //   MemberProfile prof = new MemberProfile(aname, teamname,location,phoneno,id);

//        databaseReference.setValue(prof).addOnCompleteListener(new OnCompleteListener<Void>() {
      //      @Override
    //        public void onComplete(@NonNull Task<Void> task) {
  //              Toast.makeText(getApplicationContext(),"Profile updated Successfully",Toast.LENGTH_SHORT).show();}
  //      });


    }
}
