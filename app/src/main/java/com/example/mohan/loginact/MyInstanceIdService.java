package com.example.mohan.loginact;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Objects;

public class MyInstanceIdService extends FirebaseMessagingService
{


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage )
    {
            String title = Objects.requireNonNull(remoteMessage.getNotification()).getTitle();
            String body = remoteMessage.getNotification().getBody();

            MyNotificationManager.getInstance(getApplicationContext())
                    .displayNotification(title,body);
    }
}
