package com.example.mohan.loginact;

public class Profile {
    String dispname;
    String tn;
    String ts;
    String loc;
    String tm;
    String pno;
    String userid;

    public String getDispname() {
        return dispname;
    }

    public String getTn() {
        return tn;
    }

    public String getTs() {
        return ts;
    }

    public String getLoc() {
        return loc;
    }

    public String getTm() {
        return tm;
    }

    public String getPno() {
        return pno;
    }

    public String getUserid() {
        return userid;
    }

    public Profile(){

    }

    public Profile(String dispname, String tn, String ts, String loc, String tm, String pno, String userid) {
        this.dispname = dispname;
        this.tn = tn;
        this.ts = ts;
        this.loc = loc;
        this.tm = tm;
        this.pno = pno;
        this.userid = userid;
    }
}
