package com.example.mohan.loginact;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;


/**
 * Created by User on 1/1/2018.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{

    private static final String TAG = "RecyclerViewAdapter";

    private ArrayList<String> category = new ArrayList<>();
    private ArrayList<String> mImages = new ArrayList<>();
    private ArrayList<String> mstatus = new ArrayList<>();
    private Context mContext;
    private int flag;

    public RecyclerViewAdapter(Context context, ArrayList<String> imageNames, ArrayList<String> images , ArrayList<String> status,int flag) {
        category = imageNames;
        mImages = images;
        mstatus = status;
        mContext = context;
        this.flag = flag;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.thread, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        Picasso.get().load(mImages.get(position)).fit().centerCrop().into(holder.image);

        holder.category.setText(category.get(position));

        holder.status.setText(mstatus.get(position));

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: clicked on: " + category.get(position));

               // Toast.makeText(mContext, category.get(position), Toast.LENGTH_SHORT).show();
                if(flag == 0) {
                    Intent intent = new Intent(mContext, ViewThread.class);
                    intent.putExtra("image_url", mImages.get(position));
                    intent.putExtra("image_name", category.get(position));
                    intent.putExtra("thread_status",mstatus.get(position) );
                    mContext.startActivity(intent);
                }
                else {
                    Intent intent = new Intent(mContext, ViewThread_scheduled.class);
                    intent.putExtra("image_url", mImages.get(position));
                    intent.putExtra("image_name", category.get(position));
                    mContext.startActivity(intent);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return category.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView image;
        TextView category;
        TextView status;
        LinearLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.imageView);
            category = itemView.findViewById(R.id.editText);
            status = itemView.findViewById(R.id.Status);
            parentLayout = itemView.findViewById(R.id.parent);
        }
    }
}














