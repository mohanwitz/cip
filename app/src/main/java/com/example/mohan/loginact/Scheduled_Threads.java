package com.example.mohan.loginact;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class Scheduled_Threads extends AppCompatActivity {

    RecyclerView recyclerView;
    LinearLayout parentLinearLayout;
    DatabaseReference threadRef;
    DatabaseReference myref;

    private ArrayList<String> images = new ArrayList<>();
    private ArrayList<String> names = new ArrayList<>();
    private ArrayList<String> status = new ArrayList<>();

    FirebaseAuth mAuth;
    private StorageReference mStorage;
    private FirebaseStorage storage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scheduled__threads);

        mAuth = FirebaseAuth.getInstance();

        threadRef = FirebaseDatabase.getInstance().getReference();
        myref = FirebaseDatabase.getInstance().getReference();

        FirebaseUser user = mAuth.getCurrentUser();

        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        parentLinearLayout = (LinearLayout) findViewById(R.id.parent);



        threadRef.child("scheduled_threads").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                insert(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    public void insert(DataSnapshot dataSnapshot)
    {
        names.clear();
        images.clear();
        FirebaseUser user = mAuth.getCurrentUser();
        String userID = user.getUid();
        String scheduledthreads;

        for(DataSnapshot ds : dataSnapshot.getChildren()){

            if(ds.getValue(scheduled_class.class).userId.equals(userID)) {

                scheduledthreads = ds.getKey();
                Log.d("Scheduled_key",scheduledthreads);

                final String finalScheduledthreads1 = scheduledthreads;

                myref.child("Threads").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        for(DataSnapshot ds1 : dataSnapshot.getChildren()) {

                            if (ds1.getKey().equals(finalScheduledthreads1)) {

                                Log.d("Scheduled_key123:", finalScheduledthreads1);

                                Thread_class Tinfo = new Thread_class();
                                Tinfo.setCategory(ds1.getValue(Thread_class.class).getCategory());
                                Tinfo.setImageurl(ds1.getValue(Thread_class.class).getImageurl());
                                Tinfo.setStatus(ds1.getValue(Thread_class.class).getStatus());
                                Tinfo.setStatus(ds1.getValue(Thread_class.class).getStatus());


                                names.add(Tinfo.getCategory());
                                images.add(Tinfo.getImageurl());
                                status.add(Tinfo.getStatus());

                            }
                        }
                        initrecylerview();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }

                });

            }
        }

    }

    public void initrecylerview()
    {

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        RecyclerViewAdapter adaptar = new RecyclerViewAdapter(this,names,images,status,1);
        recyclerView.setAdapter(adaptar);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {

        final MenuInflater inflater = getMenuInflater();
        mAuth = FirebaseAuth.getInstance();
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = mAuth.getCurrentUser();
        final String[] userID = {user.getUid()};

        myRef.child("Adminoruser").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                FirebaseUser user = mAuth.getCurrentUser();
                userID[0] = user.getUid();


                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    if (ds.getKey().equals(userID[0])) {

                        if(ds.getValue().equals("TeamMember")) {

                            inflater.inflate(R.menu.menu,menu);

                        }
                        else {
                            inflater.inflate(R.menu.leadermenu,menu);
                        }


                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case R.id.menuLogout:
                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(new Intent(this,MainActivity.class));
                break;

            case R.id.Profile:

                startActivity(new Intent(this,helperprofile.class));
                break;
            case R.id.Mythreads:

                startActivity(new Intent(getApplicationContext(),MyThread.class));
                break;

            case R.id.scheduled:
                startActivity(new Intent(getApplicationContext(),Scheduled_Threads.class));
                break;
            case R.id.Help:
                startActivity(new Intent(getApplicationContext(),Help.class));
                break;


        }
        return true;
    }

}
