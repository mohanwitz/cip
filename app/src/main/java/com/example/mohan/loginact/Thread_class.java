package com.example.mohan.loginact;

public class Thread_class {

    public String userid;
    public String category;
    public String imageurl;
    public String status;


    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Thread_class(String userid, String category, String imageurl, String status) {
        this.userid = userid;
        this.category = category;

        this.imageurl = imageurl;
        this.status = status;
    }

    public Thread_class() {
    }
}


