package com.example.mohan.loginact;

public class User_class {

    public String dispname;
    public String loc;
    public String pno;
    public String tm;
    public String tn;
    public String ts;
    public String userId;

    public String getDispname() {
        return dispname;
    }

    public String getLoc() {
        return loc;
    }

    public String getPno() {
        return pno;
    }

    public String getTm() {
        return tm;
    }

    public String getTn() {
        return tn;
    }

    public String getTs() {
        return ts;
    }


    public String getUserId() {
        return userId;
    }

    public void setDispname(String dispname) {
        this.dispname = dispname;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public void setPno(String pno) {
        this.pno = pno;
    }

    public void setTm(String tm) {
        this.tm = tm;
    }

    public void setTn(String tn) {
        this.tn = tn;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

  public   User_class(String dispname ,String loc, String pno, String tm, String tn , String ts , String userId)
    {
        this.dispname = dispname;
        this.loc = loc;
        this.pno = pno;
        this.tm = tm;
        this.tn = tn;
        this.ts = ts;
        this.userId = userId;

    }

   public User_class()
    {

    }
}
