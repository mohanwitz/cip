package com.example.mohan.loginact;

public class User_class_teammember {


    public String dispname;
    public String loc;
    public String pno;

    public String tn;

    public String userId;

    public String getDispname() {
        return dispname;
    }

    public String getLoc() {
        return loc;
    }

    public String getPno() {
        return pno;
    }



    public String getTn() {
        return tn;
    }



    public String getUserId() {
        return userId;
    }

    public void setDispname(String dispname) {
        this.dispname = dispname;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public void setPno(String pno) {
        this.pno = pno;
    }



    public void setTn(String tn) {
        this.tn = tn;
    }



    public void setUserId(String userId) {
        this.userId = userId;
    }

   public User_class_teammember(String dispname ,String loc, String pno,  String tn , String userId)
    {
        this.dispname = dispname;
        this.loc = loc;
        this.pno = pno;

        this.tn = tn;

        this.userId = userId;

    }

    public  User_class_teammember()
    {

    }
}
