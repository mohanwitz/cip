package com.example.mohan.loginact;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.LocationCallback;
import com.google.firebase.FirebaseError;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.Objects;

public class ViewThread extends AppCompatActivity {

    ImageView image;
    TextView text;

    private DatabaseReference myRef,myRef2;

    String imageUrl;
    FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthListener;

    String catName;
           String imageUrl2;
           String keys;
           String userID;
            int flag = 0, flag1 = 0;
           double latitude;
           double longitude;
           Intent i;
          private Button button,scheduleb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_thread);
        mAuth = FirebaseAuth.getInstance();

        FirebaseUser user = mAuth.getCurrentUser();
        assert user != null;
        userID = user.getUid();


        button = (Button) findViewById(R.id.location);
        scheduleb = (Button) findViewById(R.id.schedule);

        myRef =FirebaseDatabase.getInstance().getReference();
        myRef2 = FirebaseDatabase.getInstance().getReference();
        DatabaseReference ref1 = myRef.child("Adminoruser");

        myRef.child("Threads").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                general(dataSnapshot);
                getlatlong(myRef.child("thread_location"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        myRef2.child("Adminoruser").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren())
                {
                    if(ds.getValue().equals("TeamMember") && ds.getKey().equals(userID))
                    {

                        // scheduleb.setEnabled(false);
                        flag = 1;
                        break;

                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        scheduleb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 i = new Intent(getApplicationContext(),selectTimeDate.class);
               i.putExtra("Thread_id",keys);

                myRef.child("Adminoruser").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for(DataSnapshot ds : dataSnapshot.getChildren())
                        {
                            if(ds.getValue().equals("TeamMember") && ds.getKey().equals(userID))
                            {

                               // scheduleb.setEnabled(false);
                                flag = 1;
                                break;

                            }
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                    if(flag == 0 ) {
                        startActivity(i);

                    }
                    else
                        Toast.makeText(getApplicationContext()," you are not admin!",Toast.LENGTH_LONG).show();


            }
        });



       button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String urlAddress = "http://maps.google.com/maps?q="+ latitude  +"," + longitude +"("+ "Here" + ")&iwloc=A&hl=es";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlAddress));
                try {
                    startActivity(intent);
                }
                catch(ActivityNotFoundException ex)
                {
                    try
                    {
                        Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlAddress));
                        startActivity(unrestrictedIntent);
                    }
                    catch(ActivityNotFoundException innerEx)
                    {
                        Toast.makeText(getApplicationContext(),"Please install a maps application",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }



    private void getlatlong(DatabaseReference ref)
    {


        GeoFire geoFire = new GeoFire(ref);
        geoFire.getLocation(keys, new LocationCallback() {
            @Override
            public void onLocationResult(String key, GeoLocation location) {
                if (location != null) {
                    latitude = location.latitude;
                    longitude = location.longitude;
                  //  Toast.makeText(ViewThread.this,"lat"+latitude+ "long"+ longitude, Toast.LENGTH_SHORT).show();

                } else {
                    //System.out.println(String.format("There is no location for key %s in GeoFire", key));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //System.err.println("There was an error getting the GeoFire location: " + databaseError);
            }
        });
    }


    private void general(DataSnapshot dataSnapshot) {


        if(getIntent().hasExtra("image_url") && getIntent().hasExtra("image_name")){

            imageUrl = getIntent().getStringExtra("image_url");
            catName = getIntent().getStringExtra("image_name");

            setImage(imageUrl,catName);

                  for (DataSnapshot ds : dataSnapshot.getChildren())
                  {
                      Thread_class Tinfo = new Thread_class();
                      Tinfo.setImageurl(ds.getValue(Thread_class.class).getImageurl());
                      if(Tinfo.getImageurl().equals(imageUrl)) {
                         keys = ds.getKey();
                         break;
                      }

                  }


            // Toast.makeText(ViewThread.this, keys, Toast.LENGTH_SHORT).show();

        }
    }

    private void setImage(String imageUrl, String catName) {
        image = (ImageView)findViewById(R.id.image);
        text = (TextView)findViewById(R.id.category);
        text.setText(catName);
        Picasso.get().load(imageUrl).into(image);

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {

        final MenuInflater inflater = getMenuInflater();
        mAuth = FirebaseAuth.getInstance();
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = mAuth.getCurrentUser();
        final String[] userID = {user.getUid()};

        myRef.child("Adminoruser").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                FirebaseUser user = mAuth.getCurrentUser();
                userID[0] = user.getUid();


                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    if (ds.getKey().equals(userID[0])) {

                        if(ds.getValue().equals("TeamMember")) {

                            inflater.inflate(R.menu.menu,menu);

                        }
                        else {
                            inflater.inflate(R.menu.leadermenu,menu);
                        }


                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case R.id.menuLogout:
                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(new Intent(this,MainActivity.class));
                break;

            case R.id.Profile:

                startActivity(new Intent(this,helperprofile.class));
                break;
            case R.id.Mythreads:

                startActivity(new Intent(getApplicationContext(),MyThread.class));
                break;

            case R.id.scheduled:
                startActivity(new Intent(getApplicationContext(),Scheduled_Threads.class));
                break;

            case R.id.Help:
                startActivity(new Intent(getApplicationContext(),Help.class));


        }
        return true;
    }

}
