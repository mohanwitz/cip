package com.example.mohan.loginact;

import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextClock;
import android.widget.TextView;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.RequestResult;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Info;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;

public class View_appoinment extends AppCompatActivity {

    double latitude,longitude;
    TextView result,tokencount;
    String did,time,tokeno,date,characteristics,chronic;
    DatabaseReference myRef,cancelref;
    Button cancel,map;
    String pid,pid2;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_appoinment);


        did = getIntent().getStringExtra("did");
        date = getIntent().getStringExtra("date");
        time = getIntent().getStringExtra("time");
        tokeno = getIntent().getStringExtra("tokeno");


        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        pid2 = user.getUid();

        myRef = FirebaseDatabase.getInstance().getReference();
        cancelref = FirebaseDatabase.getInstance().getReference();

        map = (Button) findViewById(R.id.map2);
        result = (TextView)findViewById(R.id.result);
        tokencount = (TextView) findViewById(R.id.livecount);
        cancel = (Button) findViewById(R.id.cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelappoinment();
            }
        });

        myRef.child("tokencount").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                show_appoinment(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), MapsActivity.class);
                startActivity(intent);

            }
        });

        final String[] distance = new String[1];
        final String[] duration = new String[1];


        GPSTracker gps = new GPSTracker(View_appoinment.this);
        if(gps.canGetLocation())
        {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        }

        String serverKey = "AIzaSyDfBU93mmTnnu34vMDmcOQFkX8oJ7ifNRs";
        LatLng origin = new LatLng(13.012303, 80.240315);
        LatLng destination = new LatLng(13.002404,80.2454538);

        //result.setText("Latitude"+Double.toString(latitude)+"\nLongitude"+Double.toString(longitude));


        GoogleDirection.withServerKey(serverKey)
                .from(origin)
                .to(destination)
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {
                        // Do something here
                        String status = direction.getStatus();
                        result.setText(status);
                        if(status.equals(RequestResult.OK)) {
                            Route route = direction.getRouteList().get(0);
                            Leg leg = route.getLegList().get(0);
                            Info distanceInfo = leg.getDistance();
                            Info durationInfo = leg.getDuration();
                            distance[0] = distanceInfo.getText();
                            duration[0] = durationInfo.getText();

                            StringTokenizer st1 = new StringTokenizer(duration[0]," ");
                            int delaytime = Integer.parseInt(st1.nextToken());

                            StringTokenizer st2 = new StringTokenizer(time,".");
                            int h = Integer.parseInt(st2.nextToken());
                            int m = Integer.parseInt(st2.nextToken());

                            SimpleDateFormat sdf1 = new SimpleDateFormat("HH.mm");

                            Calendar calendar = new GregorianCalendar(2013,1,28,h,m,00);

                            calendar.add(Calendar.MINUTE,-delaytime);

                            time =sdf1.format(calendar.getTime()).toString();

                            //String did,time,tokeno,date;

                            result.setText("Distance = "+distance[0]+"\nDuration = "+duration[0]+"\nTime to leave = "+time+"\nTokeno = "+tokeno+"\nDate = "+date);

                            try {
                                Thread.sleep(5000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            return;
                        } else if(status.equals(RequestResult.NOT_FOUND)) {
                            // Do something
                        }
                    }
                    @Override
                    public void onDirectionFailure(Throwable t) {
                        // Do something here
                    }
                });
        //distanceandtime.setText("Distance = "+distance[0]+"\nDuration = "+duration[0]);*/



    }

    private void cancelappoinment() {

        myRef.child("PatientAppointment").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot ds : dataSnapshot.getChildren())
                {
                    tokencount.setText(ds.getKey());
                    if(ds.getKey().equals(pid2))
                    {

                        for(DataSnapshot temp1 : ds.getChildren())
                        {
                            if(temp1.getKey().equals(date))
                            {
                                temp1.getRef().removeValue();
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        final String[] constime = new String[1];
        myRef.child("DoctorsAppointment").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                for(DataSnapshot ds : dataSnapshot.getChildren())
                {
                    if(ds.getKey().equals(date))
                    {
                        for(DataSnapshot temp1 : ds.getChildren())
                        {
                            if(temp1.getKey().equals(did))
                            {
                                for(DataSnapshot temp2 : temp1.getChildren())
                                {
                                    if(temp2.getKey().equals(tokeno))
                                    {
                                        temp2.getRef().removeValue();
                                        appointmentdetails detials = new appointmentdetails();
                                        detials.setConstime(temp2.getValue(appointmentdetails.class).getConstime());
                                        constime[0] = detials.getConstime();
                                    }
                                    if(Integer.parseInt(temp2.getKey())>Integer.parseInt(tokeno))
                                    {
                                        tokencount.setText(temp2.getKey());
                                        appointmentdetails detials = new appointmentdetails();
                                        detials.setCharc(temp2.getValue(appointmentdetails.class).getCharc());
                                        detials.setChronic(temp2.getValue(appointmentdetails.class).getChronic());
                                        detials.setConstime(temp2.getValue(appointmentdetails.class).getConstime());
                                        detials.setPid(temp2.getValue(appointmentdetails.class).getPid());
                                        detials.setToken(temp2.getValue(appointmentdetails.class).getToken());


                                        StringTokenizer st1 = new StringTokenizer(temp2.getValue(appointmentdetails.class).getTime(),".");

                                        int h = Integer.parseInt(st1.nextToken());
                                        int m = Integer.parseInt(st1.nextToken());

                                        SimpleDateFormat sdf1 = new SimpleDateFormat("HH.mm");

                                        Calendar calendar = new GregorianCalendar(2013,1,28,h,m,00);

                                        calendar.add(Calendar.MINUTE, -Integer.parseInt(constime[0]));
                                        time =sdf1.format(calendar.getTime()).toString();

                                        String tokeno1 = Integer.toString(Integer.parseInt(detials.getToken())-1);
                                        pid = detials.getPid();
                                        String consultingtime = detials.getConstime();
                                        characteristics = detials.getCharc();
                                        chronic = detials.getChronic();

                                        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("DoctorsAppointment");
                                        appointmentdetails detials1 = new appointmentdetails(pid,time,tokeno1,consultingtime,chronic,characteristics,pid2);
                                        databaseReference.child(date).child(did).child(tokeno1).setValue(detials1);
                                        temp2.getRef().removeValue();
                                    }


                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        finish();
        Intent intent = new Intent(getBaseContext(), Home.class);
        startActivity(intent);


    }

    private void show_appoinment(DataSnapshot dataSnapshot) {

        for(DataSnapshot ds: dataSnapshot.getChildren())
        {
            if(ds.getKey().equals(did))
            {
                livecount count = new livecount();
                count.setLive(ds.getValue(livecount.class).getLive());
                String temp;
                temp = count.getLive();
                tokencount.setText("Live_Token_Count : "+temp);
            }
        }
    }
}
