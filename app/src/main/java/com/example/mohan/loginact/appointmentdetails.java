package com.example.mohan.loginact;

public class appointmentdetails {
    String pid;
    String time;
    String token;
    String constime;
    String chronic;
    String charc;
    String id;

    public appointmentdetails(String pid, String time, String token, String constime, String chronic, String charc, String id) {
        this.pid = pid;
        this.time = time;
        this.token = token;
        this.constime = constime;
        this.chronic = chronic;
        this.charc = charc;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChronic() {
        return chronic;
    }

    public void setChronic(String chronic) {
        this.chronic = chronic;
    }

    public String getCharc() {
        return charc;
    }

    public void setCharc(String charc) {
        this.charc = charc;
    }
    public String getConstime() {
        return constime;
    }

    public void setConstime(String constime) {
        this.constime = constime;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    public appointmentdetails() {
    }
}
