package com.example.mohan.loginact;

public class doctor {
   String fname;
   String lname;
   String docid;
   String location;
   String phoneno;
   String userid;
   String experience;
   String specialization;

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public doctor(String fname, String lname, String docid, String location, String phoneno, String userid, String experience, String specialization) {
        this.fname = fname;
        this.lname = lname;
        this.docid = docid;
        this.location = location;
        this.phoneno = phoneno;
        this.userid = userid;
        this.experience = experience;
        this.specialization = specialization;
    }

    public doctor()
    {

    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getDocid() {
        return docid;
    }

    public void setDocid(String docid) {
        this.docid = docid;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
