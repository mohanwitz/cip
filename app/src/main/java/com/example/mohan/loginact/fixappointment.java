package com.example.mohan.loginact;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class fixappointment extends AppCompatActivity{

    Spinner spinner,chara,chronic,category;
    FirebaseAuth.AuthStateListener mAuthListener;
    DatabaseReference myRef;
    String userID;
    FirebaseAuth mAuth;
    final String[] docname = new String[1];
    final String[] chroniccondition = new String[1];
    final String[] characteristics = new String[1];
    final String[] doccateg = new String[1];
    Button fix;
    private  EditText editText;
    final Calendar myCalendar = Calendar.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fixappointment);
        category = (Spinner) findViewById(R.id.spinnercategory);
        spinner = (Spinner) findViewById(R.id.spinner);
        chara = (Spinner) findViewById(R.id.charac);
        chronic = (Spinner) findViewById(R.id.chroniccondition);
        mAuth = FirebaseAuth.getInstance();
        fix = (Button)findViewById(R.id.Fix);
        editText = (EditText)findViewById(R.id.Date);


        myRef = FirebaseDatabase.getInstance().getReference();

        FirebaseUser user = mAuth.getCurrentUser();
        userID = user.getUid();


        ArrayList<String> categorylist = new ArrayList<>();
        categorylist.add("Physician");
        categorylist.add("Anesthesiologists");
        categorylist.add("Neurologists");
        categorylist.add("Radiologists");
        categorylist.add("Dermatologists");
        categorylist.add("Opthalamologists");


        ArrayAdapter<String> adapter5 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,categorylist);

        category.setAdapter(adapter5);

        category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                doccateg[0] = parentView.getItemAtPosition(position).toString();

                myRef.child("Doctors").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // This method is called once with the initial value and again
                        // whenever data at this location is updated.
                        showData(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                //
            }

        });


        ArrayList<String> list = new ArrayList<>();
        list.add("yes");
        list.add("no");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,list);

        chronic.setAdapter(adapter);

        chronic.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                chroniccondition[0] = parentView.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                //
            }

        });

        ArrayList<String> list1 = new ArrayList<>();
        list1.add("mostly_physical");
        list1.add("half_physical_psychological");
        list1.add("mostly_psychological");

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,list1);

        chara.setAdapter(adapter1);

        chara.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                characteristics[0] = parentView.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                //
            }

        });




        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


        editText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(fixappointment.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        ;


        
        fix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String appdate = editText.getText().toString();
                String doctorname = docname[0];

                if(appdate.isEmpty())
                {
                    editText.setError("Please choose a Date");
                    editText.requestFocus();
                    return;
                }
                Intent intent = new Intent(getBaseContext(), fixappointment2.class);
                intent.putExtra("docname",doctorname);
                intent.putExtra("char",characteristics[0]);
                intent.putExtra("chronic",chroniccondition[0]);
                intent.putExtra("date",appdate);
                startActivity(intent);
            }
        });


        //mAuth = FirebaseAuth.getInstance();

    }



    private void updateLabel() {
        String myFormat = "MM-dd-yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        editText.setText(sdf.format(myCalendar.getTime()));
    }

    private void showData(DataSnapshot dataSnapshot) {
        ArrayList<String> list = new ArrayList<>();

        list.add("None");
        for(DataSnapshot ds : dataSnapshot.getChildren()) {

                doctor uInfo = new doctor();

                uInfo.setFname(ds.getValue(doctor.class).getFname()); //set the name
                uInfo.setSpecialization(ds.getValue(doctor.class).getSpecialization()); //set the name
                if(uInfo.getSpecialization().equals(doccateg[0]))
                {
                    list.add((String)uInfo.getFname());
                }

            }

    ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,list);

        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                docname[0] = parentView.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                //
            }

        });
    }
}
