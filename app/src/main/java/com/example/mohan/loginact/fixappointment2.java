package com.example.mohan.loginact;

import android.content.Intent;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;

import static java.sql.Types.NULL;

public class fixappointment2 extends AppCompatActivity {

    TextView view,constime;
    String docname, date, docid,pid,chronic,characteristics,age,gender,docexp;
    String time;
    int tokeno;
    FirebaseAuth.AuthStateListener mAuthListener;
    DatabaseReference databaseReference, myRef, doctorname,temp;
    FirebaseAuth mAuth;
    int consstime;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fixappointment2);




        docname = getIntent().getStringExtra("docname");
        date = getIntent().getStringExtra("date");
        chronic = getIntent().getStringExtra("chronic");
        characteristics = getIntent().getStringExtra("char");


        view = (TextView) findViewById(R.id.view);
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        pid = user.getUid();

        myRef = FirebaseDatabase.getInstance().getReference();

        myRef.child("Clients").addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot1) {

                for (DataSnapshot temp : dataSnapshot1.getChildren()) {

                    if (temp.getKey().equals(pid)) {
                        MemberProfile profile = new MemberProfile();
                        profile.setFname(temp.getValue(MemberProfile.class).getFname());
                        pid = profile.getFname();
                        break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });


        myRef = FirebaseDatabase.getInstance().getReference();
        doctorname = FirebaseDatabase.getInstance().getReference();
        temp = FirebaseDatabase.getInstance().getReference();


        doctorname.child("Doctors").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                finddocid(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        final String pid2 = user.getUid();

        myRef.child("Clients").addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot1) {

                for (DataSnapshot temp : dataSnapshot1.getChildren()) {

                    if (temp.getKey().equals(pid2)) {
                        MemberProfile profile = new MemberProfile();
                        profile.setAge(temp.getValue(MemberProfile.class).getAge());
                        profile.setGender(temp.getValue(MemberProfile.class).getGender());
                        age = profile.getAge();
                        gender = profile.getGender();
                        break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });

        myRef.child("DoctorsAppointment").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                //Toast.makeText(getApplicationContext(),age+gender+chronic+characteristics+docexp,Toast.LENGTH_LONG).show();
                new FetchWeatherData().execute();

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                bookappointment(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }

    private class FetchWeatherData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            HttpURLConnection conn = null;
            try {
                URL url = new URL("http://192.168.137.82:5000/?age="+age+"&gen="+gender+"&chron="+chronic+"&char="+characteristics+"&docexp="+docexp);
                conn = (HttpURLConnection) url.openConnection();
                try {
                    InputStreamReader input = new InputStreamReader(conn.getInputStream());
                    int temp = conn.getResponseCode();
                    BufferedReader r = new BufferedReader(input);
                    final StringBuilder total = new StringBuilder();
                    for (String line; (line = r.readLine()) != null; ) {
                        total.append(line).append('\n');
                    }
                    final String temp2 = total.toString();
                    StringTokenizer st1 = new StringTokenizer(temp2,".");
                    final String temp1 = st1.nextToken();
                    consstime = Integer.parseInt(temp1);

                } finally {
                    conn.disconnect();
                }
            } catch (Exception e) {
                Log.d("Error2", String.valueOf(e));

            }
            return null;

        }
    }

    private void findtokenandtime(DataSnapshot dataSnapshot) {

        //tokeno = 0;
        //time = "0";

        for(DataSnapshot ds : dataSnapshot.getChildren())
        {

            appointmentdetails uInfo = new appointmentdetails();
            uInfo.setTime(ds.getValue(appointmentdetails.class).getTime());
            uInfo.setToken(ds.getValue(appointmentdetails.class).getToken());
            uInfo.setConstime(ds.getValue(appointmentdetails.class).getConstime());

            int temp = Integer.parseInt(uInfo.getToken());

            String consultingtime = uInfo.getConstime();
            if(tokeno < temp)
            {
                tokeno = temp;

                StringTokenizer st1 = new StringTokenizer(ds.getValue(appointmentdetails.class).getTime(),".");

                int h = Integer.parseInt(st1.nextToken());
                int m = Integer.parseInt(st1.nextToken());

                SimpleDateFormat sdf1 = new SimpleDateFormat("HH.mm");

                Calendar calendar = new GregorianCalendar(2013,1,28,h,m,00);

                calendar.add(Calendar.MINUTE,Integer.parseInt(consultingtime));
                time =sdf1.format(calendar.getTime()).toString();


            }
        }
        ++tokeno;
        //Toast.makeText(getApplicationContext(),Integer.toString(++tokeno),Toast.LENGTH_LONG).show();
    }

    private void finddocid(DataSnapshot dataSnapshot) {

        for (DataSnapshot ds : dataSnapshot.getChildren()) {

            if (ds.getValue(doctor.class).fname.equals(docname)) {

                docid = ds.getValue(doctor.class).docid;
                docexp = ds.getValue(doctor.class).experience;


            }
        }
        temp.child("DoctorsAppointment").child(date).child(docid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                findtokenandtime(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void bookappointment(DataSnapshot dataSnapshot) {

        int flag = 0;
        for (DataSnapshot ds : dataSnapshot.getChildren()) {

            if (ds.getKey().equals(date)) {
               // Toast.makeText(getApplicationContext(), "Date is there!!", Toast.LENGTH_SHORT).show();
                flag = 1;

                for(DataSnapshot temp1 : ds.getChildren())
                {
                    if(tokeno > 10)
                    {
                        view.setText("Max Appointment reached");
                        return;
                    }

                    FirebaseUser user = mAuth.getCurrentUser();
                    String pid1 = user.getUid();

                    databaseReference = FirebaseDatabase.getInstance().getReference("DoctorsAppointment");
                    appointmentdetails detials = new appointmentdetails(pid,time,Integer.toString(tokeno),Integer.toString(consstime),chronic,characteristics,pid1);
                    databaseReference.child(date).child(docid).child(Integer.toString(tokeno)).setValue(detials);

                    view.setText("Your appointment is on "+date+"\nToken No : "+tokeno+"\nTime:"+time);
                    databaseReference = FirebaseDatabase.getInstance().getReference("PatientAppointment");
                    patientappoinmentdetials pdet = new patientappoinmentdetials(pid,docid,date,time,Integer.toString(tokeno),Integer.toString(consstime));
                    databaseReference.child(pid1).child(date).setValue(pdet);
                    return;
                }
            }
        }
        if (flag == 0) {

            FirebaseUser user = mAuth.getCurrentUser();
            String pid1 = user.getUid();

            databaseReference = FirebaseDatabase.getInstance().getReference("DoctorsAppointment");
            appointmentdetails detials = new appointmentdetails(pid,"10.00","1",Integer.toString(consstime),chronic,characteristics,pid1);
            databaseReference.child(date).child(docid).child("1").setValue(detials);
            view.setText("Your appointment is on "+date+"\nToken No : "+"1"+"\nTime:"+"10.00");


            databaseReference = FirebaseDatabase.getInstance().getReference("PatientAppointment");
            patientappoinmentdetials pdet = new patientappoinmentdetials(pid,docid,date,"10.00","1",Integer.toString(consstime));
            databaseReference.child(pid1).child(date).setValue(pdet);
        }

    }

}