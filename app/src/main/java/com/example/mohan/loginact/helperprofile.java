package com.example.mohan.loginact;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class helperprofile extends AppCompatActivity {

    FirebaseAuth mAuth;
    DatabaseReference databaseReference;

    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference myRef;
    private String userID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_helperprofile);
        databaseReference = FirebaseDatabase.getInstance().getReference("Adminoruser");
        mAuth = FirebaseAuth.getInstance();
        mAuth = FirebaseAuth.getInstance();
        myRef = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = mAuth.getCurrentUser();
        userID = user.getUid();


        myRef.child("Adminoruser").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                FirebaseUser user = mAuth.getCurrentUser();
                userID = user.getUid();

                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                    if (ds.getKey().equals(userID)) {


                        if (ds.getValue().equals("TeamMember")) {


                            startActivity(new Intent(getApplicationContext(), Memberprofile.class));
                            finish();

                        }
                        else
                        {

                            startActivity(new Intent(getApplicationContext(), Leaderprofile.class));
                            finish();

                        }

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        

    }

}

