package com.example.mohan.loginact;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class nearby_location extends AppCompatActivity {

    RecyclerView recyclerView;
    LinearLayout parentLinearLayout;
    DatabaseReference threadRef;

    private ArrayList<String> images = new ArrayList<>();
    private ArrayList<String> names = new ArrayList<>();
    private ArrayList<String> status = new ArrayList<>();

    GPSTracker gps;
    FirebaseAuth mAuth;
    private StorageReference mStorage;
    private FirebaseStorage storage;
    double latitude;
    double longitude;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    ArrayList<String> keyss= new ArrayList<>();

    @Override
    protected  void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nearby_location);


        mAuth = FirebaseAuth.getInstance();

        threadRef = FirebaseDatabase.getInstance().getReference();

        FirebaseUser user = mAuth.getCurrentUser();

        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        parentLinearLayout = (LinearLayout) findViewById(R.id.parent);


        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
        }

        gps = new GPSTracker(nearby_location.this);
        if(gps.canGetLocation())
        {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();


           // Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        }

        DatabaseReference myref = FirebaseDatabase.getInstance().getReference();
        DatabaseReference databaseReference = myref.child("thread_location");
        GeoFire geoFire = new GeoFire(databaseReference);
        GeoQuery geoQuery = geoFire.queryAtLocation(new GeoLocation(latitude, longitude), 40);
        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {


            @Override
            public void onKeyEntered(final String key, GeoLocation location) {
               // System.out.println(String.format("Key %s entered the search area at [%f,%f]", key, location.latitude, location.longitude));
                keyss.add(key);
                //Log.d("Key1234",key);
               // Toast.makeText(getApplicationContext(), key, Toast.LENGTH_LONG).show();

                threadRef.child("Threads").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        for(DataSnapshot ds : dataSnapshot.getChildren()){
                            if(ds.getKey().equals(key)) {

                                Log.d("Key321",ds.getKey());

                                Thread_class Tinfo = new Thread_class();
                                Tinfo.setCategory(ds.getValue(Thread_class.class).getCategory());
                                Tinfo.setImageurl(ds.getValue(Thread_class.class).getImageurl());
                                Tinfo.setStatus(ds.getValue(Thread_class.class).getStatus());
                                if(Tinfo.getStatus().equals("Authenticated")) {
                                    names.add(Tinfo.getCategory());
                                    images.add(Tinfo.getImageurl());
                                    status.add(Tinfo.getStatus());
                                }
                            }

                        }
                        initrecylerview();


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }

                });



            }


            @Override
            public void onKeyExited(String key) {

            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {

            }

            @Override
            public void onGeoQueryReady() {

            }

            @Override
            public void onGeoQueryError(DatabaseError error) {

            }
        });



    }

    public void initrecylerview()
    {

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        RecyclerViewAdapter adaptar = new RecyclerViewAdapter(this,names,images,status,0);
        recyclerView.setAdapter(adaptar);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode ==  MY_PERMISSIONS_REQUEST_LOCATION) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(this, "location permission granted", Toast.LENGTH_LONG).show();

            } else {

                Toast.makeText(this, "location permission denied", Toast.LENGTH_LONG).show();

            }

        }


    }
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {

        final MenuInflater inflater = getMenuInflater();
        mAuth = FirebaseAuth.getInstance();
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = mAuth.getCurrentUser();
        final String[] userID = {user.getUid()};

        myRef.child("Adminoruser").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                FirebaseUser user = mAuth.getCurrentUser();
                userID[0] = user.getUid();


                for(DataSnapshot ds : dataSnapshot.getChildren()) {

                    if (ds.getKey().equals(userID[0])) {

                        if(ds.getValue().equals("TeamMember")) {

                            inflater.inflate(R.menu.menu,menu);

                        }
                        else {
                            inflater.inflate(R.menu.leadermenu,menu);
                        }


                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case R.id.menuLogout:
                FirebaseAuth.getInstance().signOut();
                finish();
                startActivity(new Intent(this,MainActivity.class));
                break;

            case R.id.Profile:

                startActivity(new Intent(this,helperprofile.class));
                break;
            case R.id.Mythreads:

                startActivity(new Intent(getApplicationContext(),MyThread.class));
                break;

            case R.id.scheduled:
                startActivity(new Intent(getApplicationContext(),Scheduled_Threads.class));
                break;
            case R.id.Help:
                startActivity(new Intent(getApplicationContext(),Help.class));

        }
        return true;
    }


}
