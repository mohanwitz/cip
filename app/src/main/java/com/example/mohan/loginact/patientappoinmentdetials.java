package com.example.mohan.loginact;

public class patientappoinmentdetials {
    String pid;
    String did;
    String date;
    String time;
    String tokeno;
    String constime;

    public patientappoinmentdetials(String pid, String did, String date, String time, String tokeno, String constime) {
        this.pid = pid;
        this.did = did;
        this.date = date;
        this.time = time;
        this.tokeno = tokeno;
        this.constime = constime;
    }

    public String getConstime() {
        return constime;
    }

    public void setConstime(String constime) {
        this.constime = constime;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTokeno() {
        return tokeno;
    }

    public void setTokeno(String tokeno) {
        this.tokeno = tokeno;
    }

    public patientappoinmentdetials() {
    }
}
