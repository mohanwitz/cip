package com.example.mohan.loginact;

public class scheduled_class {

    public String time;
    public String date;
    public String userId;
    public String status;
    public String thread_id;
    public String teamname;
    public String phno;
    public String token;

    public String getTime()
    {
        return time;
    }

    public String getDate() {
        return date;
    }

    public String getUserId() {
        return userId;
    }


    public String getStatus() {
        return status;
    }

    public String getThread_id() {
        return thread_id;
    }

    public String getTeamname() {
        return teamname;
    }

    public String getPhno() {
        return phno;
    }

    public String getToken() {
        return token;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setThread_id(String thread_id) {
        this.thread_id = thread_id;
    }

    public void setTeamname(String teamname) {
        this.teamname = teamname;
    }

    public void setPhno(String phno) {
        this.phno = phno;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public scheduled_class(String time, String date, String userId, String status, String teamname, String thread_id, String phno)
    {
        this.time = time;
        this.date = date;
        this.userId = userId;
        this.status = status;
        this.thread_id = thread_id;
        this.teamname = teamname;
       this.phno = phno;

    }

    public  scheduled_class()
    {

    }
}
