package com.example.mohan.loginact;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;


    import android.app.DatePickerDialog;
        import android.app.TimePickerDialog;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.Menu;
        import android.view.MenuItem;
        import android.view.View;
        import android.widget.Button;
        import android.widget.DatePicker;
        import android.widget.EditText;
        import android.widget.TimePicker;
import android.widget.Toast;
import  android.os.Build.VERSION;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class selectTimeDate extends AppCompatActivity implements
        View.OnClickListener {

    Button btnDatePicker, btnTimePicker,button;
    EditText txtDate, txtTime;
    FirebaseAuth mAuth;
    DatabaseReference myRef,myRef2,myRef3,myRef4;
    private int mYear, mMonth, mDay, mHour, mMinute;
    String time;
    String date;
    String userID;
    String teamname;
    String phno = null;
    String thread_id;
    String posted_userId;
    String token;
    int flag = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timedate);
        mAuth = FirebaseAuth.getInstance();
        final FirebaseUser user = mAuth.getCurrentUser();
        button= (Button)findViewById(R.id.button);
        btnDatePicker=(Button)findViewById(R.id.btn_date);
        btnTimePicker=(Button)findViewById(R.id.btn_time);
        txtDate=(EditText)findViewById(R.id.in_date);
        txtTime=(EditText)findViewById(R.id.in_time);
        myRef = FirebaseDatabase.getInstance().getReference();
        myRef2 = FirebaseDatabase.getInstance().getReference();

        myRef3 = FirebaseDatabase.getInstance().getReference();
        myRef4 = FirebaseDatabase.getInstance().getReference();

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( selectTimeDate.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                 token = instanceIdResult.getToken();
                Log.e("Token",token);
            }
        });
       NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)

        {
            NotificationChannel mChannel =


                    new NotificationChannel(Constants.CHANNEL_ID, Constants.CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);

            mChannel.setDescription(Constants.CHANNEL_DESCRIPTION);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);

            assert mNotificationManager != null;
            mNotificationManager.createNotificationChannel(mChannel);
        }



        assert user != null;
        userID = user.getUid();
        Intent intent = getIntent();

        thread_id = Objects.requireNonNull(intent.getExtras()).getString("Thread_id");
        assert thread_id != null;
        btnDatePicker.setOnClickListener(this);
        btnTimePicker.setOnClickListener(this);


        myRef.child("Teamnames").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren())
                {
                    if(ds.getKey().equals(userID))
                    {

                        teamname = (String) ds.getValue();
                        Log.d("Teamname",teamname);
                        break;

                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




        myRef2.child("Threads").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren())
                {
                    if(ds.getKey().equals(thread_id))
                    {

                        Thread_class Tinfo = new Thread_class();
                        Tinfo.setUserid(ds.getValue(Thread_class.class).getUserid());
                        posted_userId = Tinfo.getUserid();
                        Log.d("Posted_id",posted_userId);
                        break;

                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });





        myRef3.child("TeamMembers").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren())
                {
                    if(ds.getKey().equals(posted_userId))
                    {
                        User_class_teammember Tinfo = new User_class_teammember();
                        Tinfo.setPno(ds.getValue(User_class_teammember.class).getPno());
                        phno = Tinfo.getPno();
                        Log.d("Phno",phno);
                        break;
                    }

                }


            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        if(phno == null)
        {
            myRef4.child("TeamLeaders").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for(DataSnapshot ds : dataSnapshot.getChildren())
                    {
                        if(ds.getKey().equals(posted_userId))
                        {
                            User_class Tinfo = new User_class();
                            Tinfo.setPno(ds.getValue(User_class.class).getPno());
                            phno = Tinfo.getPno();
                            Log.d("Phno",phno);

                            break;
                        }

                    }


                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (TextUtils.isEmpty(txtDate.getText()) || TextUtils.isEmpty(txtTime.getText()))
                {
                    Toast.makeText(selectTimeDate.this, "Please select time and date", Toast.LENGTH_SHORT).show();

                }
                    else {

                    FirebaseMessaging.getInstance().subscribeToTopic("pushNotifications")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    String msg = getString(R.string.msg_subscribed);
                                    if (!task.isSuccessful()) {
                                        msg = getString(R.string.msg_subscribe_failed);
                                    }
                                    Log.d("Subscription", msg);
                                  //  Toast.makeText(selectTimeDate.this, msg, Toast.LENGTH_SHORT).show();
                                }
                            });
                    DatabaseReference ref = myRef.child("scheduled_threads");
                    DatabaseReference ref1 = myRef.child("Threads");
                    DatabaseReference ref2 = ref1.child(thread_id);
                    DatabaseReference ref3 = ref2.child("status");
                    ref3.setValue("Scheduled");
                  ref.child(thread_id).setValue(new scheduled_class(time, date, userID, "Scheduled",teamname,thread_id,phno));

                    //Map<String, scheduled_class> users = new HashMap<>();
                    //users.put(thread_id,new scheduled_class(time, date, userID, "Scheduled",teamname,thread_id,phno));
                   // ref.setValue(users);

                    button.setEnabled(false);

                    Toast.makeText(selectTimeDate.this, "Scheduled", Toast.LENGTH_SHORT).show();


                    FirebaseMessaging.getInstance().unsubscribeFromTopic("pushNotifications")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    String msg = getString(R.string.msg_subscribed);
                                    if (!task.isSuccessful()) {
                                        msg = getString(R.string.msg_subscribe_failed);
                                    }
                                    Log.d("Subscription", msg);
                                   // Toast.makeText(selectTimeDate.this, msg, Toast.LENGTH_SHORT).show();
                                }
                            });
                   startActivity(new Intent(getApplicationContext(),Home.class));


                }



            }
        });

    }

    @Override
    public void onClick(View v) {

        if (v == btnDatePicker) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        if (v == btnTimePicker) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            txtTime.setText(hourOfDay + ":" + minute);
                                time = hourOfDay + ":" + minute;
                        }
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        }
    }
}