package com.example.mohan.loginact;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.view.View;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.RequestResult;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Info;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;


public class sendgpsservice extends Service {

    double latitude,longitude;
    DatabaseReference myRef;
    FirebaseAuth mAuth;
    String pid;
    String formattedDate;
    String did,time,tokeno,date;
    final String[] distance = new String[1];
    final String[] duration = new String[1];

    @Override

    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        myRef = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        FirebaseUser user = mAuth.getCurrentUser();
        pid = user.getUid();
        Date c = Calendar.getInstance().getTime();

        SimpleDateFormat df = new SimpleDateFormat("MM-dd-yy");
        formattedDate = df.format(c);


        final GPSTracker gps = new GPSTracker(this);

        Timer timerObj = new Timer();
        TimerTask timerTaskObj = new TimerTask() {
            public void run() {


                myRef.child("PatientAppointment").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // This method is called once with the initial value and again
                        // whenever data at this location is updated.
                        show_appoinment(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                if(gps.canGetLocation())
                {
                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();
                }

                String serverKey = "AIzaSyDfBU93mmTnnu34vMDmcOQFkX8oJ7ifNRs";
                LatLng origin = new LatLng(13.012303, 80.240315);
                LatLng destination = new LatLng(13.009293, 80.213186);



                GoogleDirection.withServerKey(serverKey)
                        .from(origin)
                        .to(destination)
                        .execute(new DirectionCallback() {
                            @Override
                            public void onDirectionSuccess(Direction direction, String rawBody) {
                                // Do something here
                                String status = direction.getStatus();
                                if(status.equals(RequestResult.OK)) {
                                    Route route = direction.getRouteList().get(0);
                                    Leg leg = route.getLegList().get(0);
                                    Info distanceInfo = leg.getDistance();
                                    Info durationInfo = leg.getDuration();

                                    distance[0] = distanceInfo.getText();
                                    duration[0] = durationInfo.getText();

                                    try {
                                        Thread.sleep(5000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    return;
                                } else if(status.equals(RequestResult.NOT_FOUND)) {

                                }
                            }
                            @Override
                            public void onDirectionFailure(Throwable t) {
                            }
                        });


                new FetchWeatherData().execute();
                new sort().execute();
            }
        };
        timerObj.schedule(timerTaskObj, 0, 5000);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private class sort extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            HttpURLConnection conn = null;
            try {
                String tempurl = "http://192.168.43.89:5000/test";
                URL url = new URL(tempurl);

                conn = (HttpURLConnection) url.openConnection();
                try {

                    InputStreamReader input = new InputStreamReader(conn.getInputStream());
                    int temp = conn.getResponseCode();
                    BufferedReader r = new BufferedReader(input);
                    final StringBuilder total = new StringBuilder();
                    for (String line; (line = r.readLine()) != null; ) {
                        total.append(line).append('\n');
                    }
                    final String temp2 = total.toString();
                    StringTokenizer st1 = new StringTokenizer(temp2,".");
                    final String temp1 = st1.nextToken();

                } finally {
                    conn.disconnect();
                }
            } catch (Exception e) {

                Log.d("Error2", String.valueOf(e));

            }
            return null;

        }
    }


    private class FetchWeatherData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            HttpURLConnection conn = null;
            try {
                String tempurl = "http://192.168.43.89:5000/?did="+did+"&time="+time+"&tokeno="+tokeno+"&date="+date+"&distance="+distance[0]+"&duration="+duration[0]+"&pid="+pid;
                URL url = new URL(tempurl);

                conn = (HttpURLConnection) url.openConnection();
                try {

                    InputStreamReader input = new InputStreamReader(conn.getInputStream());
                    int temp = conn.getResponseCode();
                    BufferedReader r = new BufferedReader(input);
                    final StringBuilder total = new StringBuilder();
                    for (String line; (line = r.readLine()) != null; ) {
                        total.append(line).append('\n');
                    }
                    final String temp2 = total.toString();
                    StringTokenizer st1 = new StringTokenizer(temp2,".");
                    final String temp1 = st1.nextToken();

                } finally {
                    conn.disconnect();
                }
            } catch (Exception e) {

                Log.d("Error2", String.valueOf(e));

            }
            return null;

        }
    }

    private void show_appoinment(DataSnapshot dataSnapshot) {

        for(DataSnapshot ds : dataSnapshot.getChildren()) {
            if(ds.getKey().equals(pid))
            {
                for(DataSnapshot temp1 : ds.getChildren())
                {
                    if(temp1.getKey().equals(formattedDate))
                    {

                        patientappoinmentdetials detials = new patientappoinmentdetials();
                        detials.setDate(temp1.getValue(patientappoinmentdetials.class).getDate());
                        detials.setTime(temp1.getValue(patientappoinmentdetials.class).getTime());
                        detials.setTokeno(temp1.getValue(patientappoinmentdetials.class).getTokeno());
                        detials.setDid(temp1.getValue(patientappoinmentdetials.class).getDid());

                        date = detials.getDate();
                        time = detials.getTime();
                        tokeno = detials.getTokeno();
                        did = detials.getDid();


                        return;
                    }

                }

            }


        }

    }

}
